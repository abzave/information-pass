import socket
import threading
import constants.server_constants as sever_constants

class Server:
    
    instance = None

    def __init__(self):
        self.__current_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.__current_socket.bind((sever_constants.HOST, sever_constants.PORT))
        self.__current_socket.listen(sever_constants.MAX_CONNECTIONS)
        self.__connections = []


    def __handler(self, connection, address):
        while True:
            data = connection.recv(sever_constants.MAX_DATA_BYTES)
            other_connections = list(filter(lambda current_connection: current_connection is not connection, self.__connections))
            list(map(lambda current_connection: current_connection.send(bytes(data)), other_connections))
            if not data:
                self.__close_connection(connection, address)
                break

    def __close_connection(self, connection, address):
        print(address, sever_constants.DISCONNECTION_MESSAGE)
        self.__connections.remove(connection)
        connection.close()

    def run(self):
        print(sever_constants.SERVER_RUNNING_MESSAGE)
        while True:
            connection, address = self.__current_socket.accept()
            self.__connections.append(connection)
            self.__create_connection_thread(connection, address)
            print(sever_constants.CONNECTION_MESSAGE, address)

    def __create_connection_thread(self, connection, address):
        connection_thread = threading.Thread(target=self.__handler, args=(connection, address))
        connection_thread.daemon = True
        connection_thread.start()

    @staticmethod
    def getInstance():
        if not Server.instance:
            Server.instance = Server()
        return Server.instance
