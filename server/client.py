import socket
import threading
import constants.server_constants as sever_constants

class Client:

    def __init__(self):
        self.__current_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.__current_socket.connect((sever_constants.HOST, sever_constants.PORT))
        self.__create_send_thread()
        self.__run()

    def __send_message(self):
        while True:
            self.__current_socket.send(bytes('Adios', 'utf-8'))

    def __run(self):
        while True:
            data = self.__current_socket.recv(sever_constants.MAX_DATA_BYTES)
            if not data:
                break
            print(str(data, 'utf-8'))

    def __create_send_thread(self):
        send_thread = threading.Thread(target=self.__send_message)
        send_thread.daemon = True
        send_thread.start()
