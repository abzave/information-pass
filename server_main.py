from server.server import Server

if __name__ == '__main__':
    server = Server.getInstance()
    server.run()